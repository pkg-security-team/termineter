termineter (1.0.6-1) unstable; urgency=medium

  * New upstream version 1.0.6
  * Drop patches (applied upstream)
  * Bump standards version (no changes needed)
  * Drop obsolete py3dist-overrides
  * Update README path (.md -> .rst)
  * Update upstream URL

 -- Arnaud Rebillout <arnaudr@kali.org>  Sun, 22 Sep 2024 17:05:23 +0700

termineter (1.0.4-3) unstable; urgency=medium

  * Fix various Python 3.12 SyntaxWarning

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 18 Sep 2024 11:45:59 +0700

termineter (1.0.4-2) unstable; urgency=medium

  [ Samuel Henrique ]
  * Add salsa-ci.yml
  * Configure git-buildpackage for Debian
  * Bump DH to 13
  * d/control: Add Rules-Requires-Root: no
  * Bump Standards-Version to 4.5.0
  * d/t/control: Add superficial restriction (closes: #969872)

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update standards version to 4.2.1, no changes needed.

 -- Samuel Henrique <samueloph@debian.org>  Tue, 08 Sep 2020 19:05:47 +0100

termineter (1.0.4-1) unstable; urgency=medium

  * Initial Debian release (closes: #905806)
  * Bump DH to 11
  * Bump Standards-Version to 4.2.0
  * Bump watch to v4
  * Switch maintenance to pkg-security
  * d/control: remove x-python3-version as oldstable satisfies it
  * d/copyright: update file
  * wrap-and-sort -a

 -- Samuel Henrique <samueloph@debian.org>  Sun, 12 Aug 2018 20:12:55 -0300

termineter (1.0.4-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Thu, 15 Mar 2018 09:38:33 +0100

termineter (1.0.2-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Update debian/copyright

 -- Sophie Brun <sophie@freexian.com>  Mon, 29 Jan 2018 13:44:03 +0100

termineter (1.0.1-0kali1) kali-dev; urgency=medium

  * Import new usptream release

 -- Sophie Brun <sophie@freexian.com>  Tue, 02 Jan 2018 11:29:49 +0100

termineter (0.2.7-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Tue, 23 May 2017 14:43:15 +0200

termineter (0.2.6-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Thu, 07 Jul 2016 11:42:23 +0200

termineter (0.2.5-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Tue, 22 Mar 2016 10:02:57 +0100

termineter (0.2.4-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Use debhelper 9
  * Add Build-Depends: dh-python, python3-dev and python3-setuptools and
    update debian/rules to use setup provided by upstream
  * Add Depends: python3-* (upstream recommends to use python3)

 -- Sophie Brun <sophie@freexian.com>  Mon, 14 Mar 2016 09:32:12 +0100

termineter (0.1.0-1kali2) kali-dev; urgency=medium

  * Update debian/watch

 -- Sophie Brun <sophie@freexian.com>  Tue, 26 Jan 2016 16:04:26 +0100

termineter (0.1.0-1kali1) kali; urgency=low

  * Updated watch file

 -- Mati Aharoni <muts@kali.org>  Sun, 12 Jan 2014 15:58:08 -0500

termineter (0.1.0-1kali0) kali; urgency=low

  * Initial release

 -- Devon Kearns <dookie@kali.org>  Wed, 26 Dec 2012 08:34:24 -0700
